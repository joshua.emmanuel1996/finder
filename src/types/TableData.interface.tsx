export interface TableData{
    dataDef: {
        headers: string[],
        columns: string[],
        override?: any
    },
    data: any[]
}