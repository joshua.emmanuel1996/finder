export default interface User{
    name: string,
    address: string,
    date_of_birth: Date,
    gender: string,
    country: string
}